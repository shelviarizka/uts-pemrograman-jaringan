package rizka.shelvia.uts

import android.app.AlertDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import cn.pedant.SweetAlert.SweetAlertDialog
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class MainActivity : AppCompatActivity(), View.OnClickListener {
    var fbaut= FirebaseAuth.getInstance()
    val Tag = "openfcm"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        menu1.setOnClickListener(this)
        maps.setOnClickListener(this)
        logout.setOnClickListener(this)



    }


    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.menu1 -> {
                val intent = Intent(this, Activity_Data::class.java)
                startActivity(intent)
                }
            R.id.maps -> {
                val intent = Intent(this, Activity_Maps::class.java)
                startActivity(intent)

            }
            R.id.logout -> {
                val intent = Intent(this, Activity_Login::class.java)
                startActivity(intent)
            }


            }
        }
    }

